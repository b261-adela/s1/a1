package com.zuitt.example;

import java.util.Scanner;
public class UserInput {
    public static void main(String[] args) {
        //Scanner is used for obtaining input from the terminal.
        //"System.in" allows us to take input from the console
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter Username: ");

        //nextLine() is to capture/save the input given by the user.
        String userName = myObj.nextLine();
        System.out.println("User name is " +userName);

    }
}
