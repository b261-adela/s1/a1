package com.zuitt.example;

import java.util.Scanner;
public class activity {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.println("First Name: ");

        String firstName = myObj.nextLine();

        System.out.println("Last Name: ");

        String lastName = myObj.nextLine();
        String fullName = firstName + " " + lastName;

        System.out.println("First Subject Grade: ");
        double firstGrade = myObj.nextDouble();

        System.out.println("Second Subject Grade: ");
        double secondGrade = myObj.nextDouble();

        System.out.println("Third Subject Grade: ");
        double thirdGrade = myObj.nextDouble();

        double value = (firstGrade + secondGrade + thirdGrade)/3;
        int average = (int)value;

        System.out.println("Good day," + fullName + ". Your grade average is: " + average);
    }
}
